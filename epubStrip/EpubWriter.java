package epubStrip;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class EpubWriter {
    static String NEW_LINE = System.getProperty("line.separator");
	public static void write(File file, String content) {
        if (!file.exists()) {
        	//<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        	//<!DOCTYPE html>
        	//<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
        	StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append(FixedVariables.XML_VERSION).append(NEW_LINE);
        	htmlBuilder.append(FixedVariables.DOCTYPE).append(NEW_LINE);
        	htmlBuilder.append(FixedVariables.HTML_DECLARATION).append(NEW_LINE);
        	htmlBuilder.append(content).append(NEW_LINE);
            htmlBuilder.append("</html>");
            writeToFile(file, htmlBuilder.toString());
        }
    }
	
	public static void writeToFile(File file, String content) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);
        } catch (IOException e) {
            //TODO: something to catch exception
        }
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                //TODO: something to catch exception
            }
        }
    }
}
