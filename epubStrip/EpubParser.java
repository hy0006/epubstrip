package epubStrip;

import java.io.File;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EpubParser {
	static String NEW_LINE = System.getProperty("line.separator");

	public static void parseBook(File dir, File dir_destination) {
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.isFile()) {
				/**
				 * TODO: Hard coded for extract page number, if author of the ebook changed the
				 * naming format, this part needs to change, need more robust way to deal with
				 * this
				 */
				String file_name = file.getName();
				if (!file_name.equals(FixedVariables.TOC_XHTML)) {
					String[] page = file.getName().split("-");
					String tmp = page[page.length - 1];
					if (tmp.contains(FixedVariables.XHTML)) {
						String[] fn = tmp.split("\\.");
						if (fn.length > 0) {
							String html_content = getContent(file);
							File tmpFile = new File(dir_destination.getAbsolutePath() + "\\" + file.getName());
							EpubWriter.write(tmpFile, html_content);
						}
					} else {
						File destination_file = new File(dir_destination.getAbsolutePath() + "\\" + file_name);
						FileUtility.copyFile(file, destination_file);
					}
				} else {
					File destination_file = new File(dir_destination.getAbsolutePath() + "\\" + file_name);
					FileUtility.copyFile(file, destination_file);
				}
			} else if (file.isDirectory()) {
				File destination_file = new File(dir_destination.getAbsolutePath() + "\\" + file.getName());
				FileUtility.copyDirectory(file, destination_file);
			}
		}
	}

	public static String getContent(File file) {
		String html_content = "";
		StringBuilder htmlBuilder = new StringBuilder();
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			doc.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
			// Get <head>
			Elements headElements = doc.getElementsByTag(FixedVariables.HEAD);
			html_content = headElements.outerHtml();
			htmlBuilder.append(html_content).append(NEW_LINE);

			
			// Add <body>
			htmlBuilder.append(FixedVariables.BODY_START).append(NEW_LINE);

			// ADD <p>
			htmlBuilder.append(FixedVariables.P_START).append(NEW_LINE);
			
			// Get all weblinks including Web, Map, and Timetable
			Elements url_elements = doc.select("a");
			htmlBuilder.append(url_elements.outerHtml()).append(NEW_LINE);

			// Get all telephone numbers
			Elements p_elements = doc.select("p");
			for (int idx = 0; idx < p_elements.size(); idx++) {
				Element p_element = p_elements.get(idx);
				if (!p_element.hasClass(FixedVariables.FOOT_STATE)) {
					Elements elements = p_element.select(FixedVariables.SPAN);
					if (elements.size() > 0) {
						int element_size = elements.size();
						for (int i = 0; i < element_size; i++) {
							Element element = elements.get(i);
							if (element.hasClass(FixedVariables.CONTENT_TELEPHONE)) {
								htmlBuilder.append(element.outerHtml()).append(NEW_LINE);
							}
						}
					}
				}
			}
			// ADD </p>
			htmlBuilder.append(FixedVariables.P_END).append(NEW_LINE);
			
			// Get all multimedia content
			// All multimedia contents (i.e. video, image, audio)
			// are recorded in hidden state
			Elements digitalElements = doc.getElementsByClass(FixedVariables.HIDDEN_STATE);
			htmlBuilder.append(digitalElements.outerHtml()).append(NEW_LINE);
			
			// Add </body>
			htmlBuilder.append(FixedVariables.BODY_END).append(NEW_LINE);
			html_content = htmlBuilder.toString();
		} catch (IOException e) {
			System.out.println("");
		}
		return html_content;
	}

	public static String getWebElements(File file) {
		String html_content = "";
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			Elements url_elements = doc.select("a");
			url_elements.size();
			// System.out.println("all urls " + url_elements.outerHtml());
			Elements p_elements = doc.select("p");
			for (int idx = 0; idx < p_elements.size(); idx++) {
				Element p_element = p_elements.get(idx);
				if (!p_element.hasClass(FixedVariables.FOOT_STATE)) {
					Elements elements = p_element.select("span");
					if (elements.size() > 0) {
						int element_size = elements.size();
						for (int i = 0; i < element_size; i++) {
							Element element = elements.get(i);
							if (element.hasClass(FixedVariables.CONTENT_TELEPHONE)) {
								System.out.println(element.outerHtml());
							}
						}
					}
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return html_content;
	}

	public static String getHeadElement(File file) {
		String html_content = "";
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			Elements headElements = doc.getElementsByTag(FixedVariables.HEAD);
			html_content = headElements.outerHtml();
			// System.out.println(html_content);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return html_content;
	}

	public static String getHiddenElements(File file) {
		/**
		 * All multimedia contents (i.e. video, image, audio) are recorded in hidden
		 * state
		 */
		String html_content = "";
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			Elements digitalElements = doc.getElementsByClass(FixedVariables.HIDDEN_STATE);
			// System.out.println(digitalElements.outerHtml());
			html_content = digitalElements.outerHtml();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return html_content;
	}

	public static void getDigitalContentPerPage(File file, String query) {
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			//
			Elements digitalElements = doc.getElementsByClass(FixedVariables.HIDDEN_STATE);
			Elements elements = digitalElements.select(query);
			if (elements.size() > 0) {
				for (Element element : elements) {
					System.out.println(element.outerHtml());
				}
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		// return content;
	}

	public static int getMaxPage(File file) {
		int max_page = 0;
		try {
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			Elements nav_pages = doc.select("nav[epub:type=\"page-list\"]");
			Elements pages = nav_pages.select("ol li a");
			max_page = Integer.valueOf(pages.get(pages.size() - 1).ownText());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		// Log.d(TAG, "min page " + max_page);
		return max_page;
	}
}
