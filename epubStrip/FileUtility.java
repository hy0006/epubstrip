package epubStrip;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Deque;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;

public class FileUtility {

	private static final int BUFFER_SIZE = 8192;

	// https://stackoverflow.com/questions/1399126/java-util-zip-recreating-directory-structure
	@SuppressWarnings("resource")
	public static boolean zip(File directory, File zipfile) throws IOException {
		boolean flag = false;
		URI base = directory.toURI();
		Deque<File> queue = new LinkedList<File>();
		queue.push(directory);
		OutputStream out = new FileOutputStream(zipfile);
		Closeable res = out;
		try {
			ZipOutputStream zout = new ZipOutputStream(out);
			res = zout;
			while (!queue.isEmpty()) {
				directory = queue.pop();
				for (File kid : directory.listFiles()) {
					String name = base.relativize(kid.toURI()).getPath();
					if (kid.isDirectory()) {
						queue.push(kid);
						name = name.endsWith("/") ? name : name + "/";
						zout.putNextEntry(new ZipEntry(name));
					} else {
						zout.putNextEntry(new ZipEntry(name));
						copy(kid, zout);
						zout.closeEntry();
					}
				}
			}
		} finally {
			res.close();
			flag = true;
			System.out.println("zip is finished !");
		}
		return flag;
	}

	private static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE];
		while (true) {
			int readCount = in.read(buffer);
			if (readCount < 0) {
				break;
			}
			out.write(buffer, 0, readCount);
		}
	}

	private static void copy(File file, OutputStream out) throws IOException {
		InputStream in = new FileInputStream(file);
		try {
			copy(in, out);
		} finally {
			in.close();
		}
	}

	@SuppressWarnings("unused")
	private static void copy(InputStream in, File file) throws IOException {
		OutputStream out = new FileOutputStream(file);
		try {
			copy(in, out);
		} finally {
			out.close();
		}
	}

	public static void moveFile(File source_file, File dest_file) {
		try {
			FileUtils.moveFile(source_file, dest_file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void moveDirectory(File source_dir, File dest_dir) {
		try {
			FileUtils.moveDirectory(source_dir, dest_dir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void copyDirectory(File source_dir, File dest_dir) {
		/*
		 * File[] files = source_dir.listFiles(); for (File file:files) { String
		 * file_name = file.getName(); File dest = new File(dest_dir.getAbsolutePath() +
		 * "\\" + source_dir.getName() + "\\" + file_name);
		 * System.out.println(dest.getAbsolutePath() + "   " + dest.getName());
		 * copyFile(file, dest); }
		 */
		// TODO: there are some problems of using above method to copy directory,
		// will get back to this if have time
		// FileUtility.copyDirectory(file, destination_file);
		try {
			FileUtils.copyDirectory(source_dir, dest_dir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean copyFile(File source, File dest) {
		try {
			try (FileInputStream sourceFile = new FileInputStream(source)) {
				FileOutputStream destinationFile = null;

				try {
					destinationFile = new FileOutputStream(dest);
					byte buffer[] = new byte[512 * 1024];
					int nbLecture;

					while ((nbLecture = sourceFile.read(buffer)) != -1) {
						destinationFile.write(buffer, 0, nbLecture);
					}
				} finally {
					destinationFile.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// https://stackoverflow.com/questions/43326525/java-unzip-folder-with-subfolders
	public static boolean unzip(String zipFilePath, String destDirectory) throws IOException {
		boolean flag = false;
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));

		ZipEntry entry = zipIn.getNextEntry();
		// iterates over entries in the zip file
		while (entry != null) {
			String filePath = destDirectory + File.separator + entry.getName();
			if (!entry.isDirectory()) {
				// if the entry is a file, extracts it
				new File(filePath).getParentFile().mkdirs();
				extractFile(zipIn, filePath);
				// System.out.println(filePath);
			} else {
				// if the entry is a directory, make the directory
				File dir = new File(filePath);
				// System.out.println(filePath);
				dir.mkdirs();
			}
			zipIn.closeEntry();
			entry = zipIn.getNextEntry();
		}
		zipIn.close();
		flag = true;
		return flag;
		//System.out.println("unzip is finished !");
	}

	private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
		byte[] bytesIn = new byte[BUFFER_SIZE];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}
}
