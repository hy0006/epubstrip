package epubStrip;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Button;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class mainScreen extends Composite {
	private Text text_epub_input;
	JFileChooser input_chooser;
	JFileChooser output_chooser;
	File file_epub_input;
	File dir_epub_output;
	private Text text_epub_output;
	private Text progressText;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(1, false));

		new mainScreen(shell, SWT.NONE);
		
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

	public mainScreen(Composite parent, int style) {
		super(parent, style);

		text_epub_input = new Text(this, SWT.BORDER);
		text_epub_input.setBounds(183, 41, 257, 25);

		input_chooser = new JFileChooser();
		// fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("EPUB3 File", "epub", "EPUB");
		input_chooser.setFileFilter(filter);

		Button btnSelectEpubFile = new Button(this, SWT.NONE);
		btnSelectEpubFile.setBounds(10, 41, 164, 25);
		btnSelectEpubFile.setText("Select EPUB file");
		btnSelectEpubFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int returnVal = input_chooser.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					file_epub_input = input_chooser.getSelectedFile();
					/*
					 * get the extension given a file name String file_name =
					 * file_epub_input.getName(); int i = file_name.lastIndexOf('.'); String format
					 * = null; if (i > 0) { format = file_name.substring(i+1); }
					 */
					text_epub_input.setText(file_epub_input.getName());
				}
			}
		});

		Label lblNextGenerationPaper = new Label(this, SWT.NONE);
		lblNextGenerationPaper.setBounds(119, 10, 207, 25);
		lblNextGenerationPaper.setText("Next Generation Paper EPUB converter");

		Label lblThis = new Label(this, SWT.BORDER | SWT.WRAP);
		lblThis.setBounds(10, 258, 430, 161);
		lblThis.setText(
				"This tool is used as a supplement tool to support process of authoring an augmented paper. The main function is to take an EPUB3 file, strip all text content (due to copyright of the ebook content). and produce a slim version of EPUB3 file for further use with Next Generation Paper mobile application.\r\n\r\n");

		output_chooser = new JFileChooser();
		output_chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		Button btnSelectOutput = new Button(this, SWT.NONE);
		btnSelectOutput.setBounds(10, 79, 164, 25);
		btnSelectOutput.setText("Select Output Dir");
		btnSelectOutput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int returnVal = output_chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					dir_epub_output = output_chooser.getSelectedFile();
					text_epub_output.setText(dir_epub_output.getAbsolutePath());
				}
			}
		});

		Button btnConvertButton = new Button(this, SWT.NONE);
		btnConvertButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				convert();
			}
		});
		btnConvertButton.setBounds(171, 122, 111, 25);
		btnConvertButton.setText("Convert");

		Button btnExitButton = new Button(this, SWT.NONE);
		btnExitButton.setBounds(365, 425, 75, 25);
		btnExitButton.setText("Exit");

		text_epub_output = new Text(this, SWT.BORDER);
		text_epub_output.setBounds(183, 79, 257, 25);
		
		progressText = new Text(this, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		progressText.setBounds(10, 153, 430, 99);
		btnExitButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.exit(0);
			}
		});

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private void convert() {
		if (file_epub_input.isFile()) {
			String dir_output_origin = dir_epub_output.getAbsolutePath() + "\\" + file_epub_input.getName().split("\\.")[0] + "_origin";			
			String dir_output_destination = dir_epub_output.getAbsolutePath() + "\\" + file_epub_input.getName().split("\\.")[0];
			File file_dir_o = new File(dir_output_origin);
			File file_dir_d = new File(dir_output_destination);
			progressText.append("Start processing...\n\n");
			if (!file_dir_d.exists()) {
				file_dir_d.mkdir();
			}
			if (!file_dir_o.exists()) {
				file_dir_o.mkdir();
				boolean unzip_flag = false;
				boolean zip_flag = false;
				try {
					unzip_flag = FileUtility.unzip(file_epub_input.getAbsolutePath(), dir_output_origin);					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (unzip_flag)
					progressText.append("Unzip epub file is finished ! \n");
				File meta = new File(dir_output_origin + "\\META-INF");
				File meta_dest = new File(dir_output_destination + "\\META-INF");
				FileUtility.moveDirectory(meta, meta_dest);
				File mimetype = new File(dir_output_origin + "\\mimetype"); 
				File mimetype_dest = new File(dir_output_destination + "\\mimetype"); 
				FileUtility.moveFile(mimetype, mimetype_dest);
				File oebps = new File(dir_output_origin + "\\OEBPS");
				File oebps_dest = new File(dir_output_destination + "\\OEBPS");
				if(!oebps_dest.exists())
					oebps_dest.mkdir();
				progressText.append("Stripping text content is starting...\n");
				EpubParser.parseBook(oebps, oebps_dest);
				progressText.append("Stripping text content is finished. \n");
				progressText.append("aBook file is being compressed...\n");
				File zipFile = new File(dir_epub_output.getAbsoluteFile() + "\\" + file_epub_input.getName());
				try {
					zip_flag = FileUtility.zip(file_dir_d, zipFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (zip_flag)
					progressText.append("aBook file is successfully created ! \n");
				// Rename it
				zipFile.renameTo(new File(dir_epub_output.getAbsoluteFile() + "\\" + file_epub_input.getName().split("\\.")[0] + ".abook"));
			}			
			else {
				System.out.println("");
			}
		}
	}
}
