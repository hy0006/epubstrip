package epubStrip;

public class FixedVariables {
    //Element in XHTML files
    public static final String BODY = "body";
    public static final String HEAD = "head";
	public static final String UTF8 = "UTF-8";
    public static final String HIDDEN_STATE = "_idGenStateHide";
    public static final String FOOT_STATE = "RUNNING-FOOT--Verso";
    public static final String STYLE_ATTR = "style";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
    public static final String IMAGE = "image";
    public static final String MAP = "map";
    public static final String WEB = "web";
    public static final String TIME = "timetable";
    public static final String PHONE = "telephone";
    public static final String TOC = "toc";
    public static final String TOC_XHTML = "toc.xhtml";
    public static final String TEXT = "text";
    public static final String XHTML = "xhtml";
    public static final String TITLE_ELT = "title";
    public static final String AUTHOR_ELT = "author";
    public static final String SRC_ATTR = "src";
    
    //Define ICONs used in the ebook
    public static final String ICON_TIMETABLE_T1 = "ICON-Timetable";
    public static final String ICON_TIMETABLE_T2 = "ICON--Timetable";
    public static final String ICON_WEB = "ICON--Web-address";
    public static final String ICON_MAP = "ICON--Map-Reference-";
    //private static final String ICON_PHONE = "ICON--Telephone";

    //Define content link used in the ebook
    public static final String CONTENT_TELEPHONE = "Content--Telephone";
    public static final String IMAGE_QUERY = "img[src~=(?i)\\.(jpe?g)]";
    public static final String VIDEO_QUERY = "source[src~=(?i)\\.(mp4)]";
    public static final String AUDIO_QUERY = "source[src~=(?i)\\.(mp3)]";
    public static final String DESCRIPTION_LIST = "DESCRIPTION_LIST";
    public static final int frame_time = 1000000; // 1 second
    
    //Define content for EpubWriter
    public static final String XML_VERSION = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
    public static final String DOCTYPE = "<!DOCTYPE html>";
    public static final String HTML_DECLARATION = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">";
    public static final String BODY_START = "<body>";
    public static final String BODY_END = "</body>";
    public static final String P_START = "<p>";
    public static final String P_END = "</p>";
    public static final String SPAN = "span";
}
