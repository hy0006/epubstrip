This is a simple program with a simple GUI written in Java to strip textual content given an epub file.
The input is an epub file, and output is another epub file without text. 

Haiyue Yuan@2018